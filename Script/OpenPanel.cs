﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPanel : MonoBehaviour
{
    public GameObject loadPanelText;
    public GameObject closePanelText;
    public GameObject Panel;
    // Start is called before the first frame update
    void Start()
    {
        loadPanelText.SetActive(false);
        closePanelText.SetActive(false);
    }
   

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            loadPanelText.SetActive(true);
            if (Input.GetButtonDown("Use"))
            {
                Panel.SetActive(true);
                loadPanelText.SetActive(false);
                closePanelText.SetActive(true);
                
            }
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Panel.SetActive(false);
                closePanelText.SetActive(false);
                
            }
        }
    }

    void OnTriggerExit()
    {
        loadPanelText.SetActive(false);
        closePanelText.SetActive(false);
    }
    public void Close()
    {
        Panel.SetActive(false);
    }
}
