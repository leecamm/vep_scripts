﻿using TMPro;
using UnityEngine;

public class UserInfoPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI selfEmail;
    [SerializeField] private TextMeshProUGUI selfUsername;
    private void Start()
    {
        selfEmail.text = Client.Instance.self.Email;
        selfUsername.text = Client.Instance.self.Username;
    }
}
