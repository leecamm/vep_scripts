﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{

    public void Load()
    {
        SceneManager.LoadScene("menu");
    }
}