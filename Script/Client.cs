﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Client : MonoBehaviour
{
    public static Client Instance { private set; get; }

    private const int MAX_USER = 100;
    private const int PORT = 26000;
    private const int WEB_PORT = 26001;
    private const int BYTE_SIZE = 1024;
    private const string SERVER_IP = "127.0.0.1";

    private byte reliableChannel;
    private int connectionId;
    private int hostId;
    private byte error;

    public Account self;
    private string token;
    private bool isStart;
   

    #region Monobehaviour
    private void Start()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
        Init();
    }
    #endregion

    private void Update()
    {
        UpdateMessagePump();
    }

    public void Init()
    {
        NetworkTransport.Init();

        ConnectionConfig cc = new ConnectionConfig();
        reliableChannel = cc.AddChannel(QosType.Reliable);

        HostTopology topo = new HostTopology(cc, MAX_USER);

        //Client code only
        hostId = NetworkTransport.AddHost(topo, 0);

#if UNITY_WEBGL && !UNITY_EDITOR
        //Web Client
        connectionId = NetworkTransport.Connect(hostId, SERVER_IP, WEB_PORT, 0, out error);
        Debug.Log("Connecting from web");
#else
        //Standalone Client
        connectionId = NetworkTransport.Connect(hostId, SERVER_IP, PORT, 0, out error);
        Debug.Log("Connecting from standalone");
#endif
        Debug.Log(string.Format("Attemping to connect on {0}...", SERVER_IP));
        isStart = true;
    }

    public void Shutdown()
    {
        isStart = false;
        NetworkTransport.Shutdown();
    }

    public void UpdateMessagePump()
    {
        if (!isStart)
            return;

        int recHostId;              // Is this from Web or standalone? (Which platform is this coming from?)
        int connectionId;           // Which user is sending me this?
        int channelId;               // Which lane is he sending that message from?

        byte[] recBuffer = new byte[BYTE_SIZE];
        int dataSize;

        NetworkEventType type = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, BYTE_SIZE, out dataSize, out error);
        switch (type)
        {
            case NetworkEventType.Nothing:
                break;

            case NetworkEventType.ConnectEvent:
                Debug.Log("We have connected to the server!");
                break;

            case NetworkEventType.DisconnectEvent:
                Debug.Log("We have been disconnected!");
                break;

            case NetworkEventType.DataEvent:
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream ms = new MemoryStream(recBuffer);
                NetMsg msg = (NetMsg)formatter.Deserialize(ms);

                OnData(connectionId, channelId, recHostId, msg);
                break;

            default:
            case NetworkEventType.BroadcastEvent:
                Debug.Log("Unexpected network event type");
                break;
        }
    }

    #region OnData
    private void OnData(int cnnId, int channelId, int recHostId, NetMsg msg)
    {
        switch (msg.OP)
        {
            case NetOP.None:
                Debug.Log("Unexpected NETOP");
                break;

            case NetOP.OnCreateAccount:
                OnCreateAccount((Net_OnCreateAccount)msg);
                break;

            case NetOP.OnLoginRequest:
                OnLoginRequest((Net_OnLoginRequest)msg);
                break;
        }
    }
    private void OnCreateAccount(Net_OnCreateAccount oca)
    {
        LobbyScene.Instance.EnableInput();
        LobbyScene.Instance.ChangeAuthenticationMessage(oca.Information);
    }
    private void OnLoginRequest(Net_OnLoginRequest olr)
    {
        LobbyScene.Instance.ChangeAuthenticationMessage(olr.Information);

        if(olr.Success != 1)
        {
            //Unable to login
            LobbyScene.Instance.EnableInput();

        }
        else
        {
            //Successful login

            //This is where the login userdata is saved!
            self = new Account();
            self.ActiveConnection = olr.ConnectionId;
            self.Username = olr.Username;
            self.Email = olr.Email;

            token = olr.Token;

            SceneManager.LoadScene("Menu");
        }
    }

    #endregion

    #region Send
    public void SendServer(NetMsg msg)
    {
         
            //This is where store the data
            byte[] buffer = new byte[BYTE_SIZE];
        
            //This is where you would transfer data into a byte[]
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream(buffer);
            formatter.Serialize(ms, msg);
            
        
        NetworkTransport.Send(hostId, connectionId, reliableChannel, buffer, BYTE_SIZE, out error);
    }
    public void SendCreateAccount(string username, string password, string email)
    {

        if (!Utility.IsUsername(username)) 
        {
            //Invalid username
            LobbyScene.Instance.ChangeAuthenticationMessage("Username is invalid");
            LobbyScene.Instance.EnableInput();
            return;
        }

        if (!Utility.IsEmail(email)) //If user input wrong type of email
        {
            //Invalid email
            LobbyScene.Instance.ChangeAuthenticationMessage("Email is invalid");
            LobbyScene.Instance.EnableInput();
            return;
        }

        if (password == null || password == "")
        {
            //Invalid Password
            LobbyScene.Instance.ChangeAuthenticationMessage("Password is empty");
            LobbyScene.Instance.EnableInput();
            return;
        }

        Net_CreateAccount ca = new Net_CreateAccount();
        ca.Username = username;
        ca.Password = Utility.Sha256FromString(password);
        ca.Email = email;

        LobbyScene.Instance.ChangeAuthenticationMessage("Sending request...");
        SendServer(ca);
    }
    public void SendLoginRequest(string usernameOrEmail, string password)
    {
        if (!Utility.IsUsername(usernameOrEmail) && !Utility.IsEmail(usernameOrEmail))
        {
            //Invalid username or email
            LobbyScene.Instance.ChangeAuthenticationMessage("Email or Username is invalid");
            LobbyScene.Instance.EnableInput();
            return;
        }

        if (password == "")
        {
            //Invalid Password
            LobbyScene.Instance.ChangeAuthenticationMessage("Password is empty ");
            LobbyScene.Instance.EnableInput();
            return;
        }
        if (!Utility.IsUsername(password))
        {
            //Invalid password
            LobbyScene.Instance.ChangeAuthenticationMessage("Wrong password ");
            LobbyScene.Instance.EnableInput();
            return;
        }

      
        Net_LoginRequest lr = new Net_LoginRequest();
        lr.UsernameOrEmail = usernameOrEmail;
        lr.Password = Utility.Sha256FromString(password);

        LobbyScene.Instance.ChangeAuthenticationMessage("Sending login request...");
        SendServer(lr);
    }
    #endregion

}
