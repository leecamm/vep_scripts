﻿using TMPro;
using UnityEngine;

public class WelcomeScene : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI selfInformation;
    private void Start()
    {
        selfInformation.text = Client.Instance.self.Username;
    }

}
