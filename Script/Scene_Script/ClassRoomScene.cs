﻿using TMPro;
using UnityEngine;

public class ClassRoomScene : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI selfInformation;
    private void Start()
    {
        selfInformation.text = Client.Instance.self.Username;
    }

}