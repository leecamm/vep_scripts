﻿using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour
{
    float speed = 5.0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.eulerAngles += speed * new Vector3(x: -Input.GetAxis("Mouse Y"), y: Input.GetAxis("Mouse X"), z: 0);
        }
       
    }
    
}
