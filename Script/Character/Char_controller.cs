﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Char_controller : MonoBehaviour
{
    //Settings
    public float speed = 15;
    float rotSpeed = 60;
    float rot = 0f;
    float gravity = 30;
    float jumpSpeed = 10;
    

    //Movement infomation
    Vector3 moveDir = Vector3.zero;
    CharacterController controller;
    Animator anim;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (controller.isGrounded)
        {
            //walking
            if(Input.GetKey(KeyCode.W))
            {
                anim.SetInteger("condition", 1);
                moveDir = new Vector3(0, 0, 1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
            }
            if(Input.GetKeyUp(KeyCode.W))
            {
                anim.SetInteger("condition", 0);
                moveDir = new Vector3(0, 0, 0);

            }
            if(Input.GetKey(KeyCode.S))
            {
                anim.SetInteger("condition", 1);
                moveDir = new Vector3(0, 0, -1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
            }
            if(Input.GetKeyUp(KeyCode.S))
            {
                anim.SetInteger("condition", 0);
                moveDir = new Vector3(0, 0, 0);
            }
            

        }
        rot += Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3(0, rot, 0);

        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * Time.deltaTime);

        

        //jumping
        if (Input.GetKey(KeyCode.Space))
        {
            anim.SetInteger("condition", 1);
            moveDir = new Vector3(0, 1, 2);
            moveDir *= jumpSpeed;
            moveDir = transform.TransformDirection(moveDir);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            anim.SetInteger("condition", 0);
            moveDir = new Vector3(0, 0, 0);
        }

    
        

    }
}
