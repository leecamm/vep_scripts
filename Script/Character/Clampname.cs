﻿using UnityEngine;
using TMPro;

public class Clampname : MonoBehaviour
{
    public TextMeshProUGUI nameLabel;
 

    // Update is called once per frame
    void Update()
    {
        Vector3 namePos = Camera.main.WorldToScreenPoint(this.transform.position);
        nameLabel.transform.position = namePos;
    }
}
