﻿[System.Serializable]
// What clients will be received 
// Server -> Clients
public class Net_OnLoginRequest : NetMsg
{
    public Net_OnLoginRequest()
    {
        OP = NetOP.OnLoginRequest;
    }
    public byte Success { set; get; }
    public string Information { set; get; }

    public int ConnectionId { set; get; }
    public string Username { set; get; }
    public string Email { set; get; }
    public string Discriminator { set; get; } // For Example: leecamm #0001
    public string Token { set; get; }
}
