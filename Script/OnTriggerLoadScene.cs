﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class OnTriggerLoadScene : MonoBehaviour
{
    public GameObject loadSceneText;
    public string sceneToLoad;

    
    // Start is called before the first frame update
    void Start()
    {
        loadSceneText.SetActive(false);
    }

    // Update is called once per frame
    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            loadSceneText.SetActive(true);
            if(loadSceneText.activeInHierarchy == true && Input.GetButtonDown("Use"))
            {
                SceneManager.LoadScene(sceneToLoad);
                
            }
        }
    }
    
    void OnTriggerExit()
    {
        loadSceneText.SetActive(false);
    }
}
